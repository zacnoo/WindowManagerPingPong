/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package higuys;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Toolkit;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *
 * @author zacnoo
 */
public class Ball {

	private int vSpeed, hSpeed;

	private JFrame frame;
	private JPanel panel;


	public Ball(int x, int y, int width, int height) {
		frame = new JFrame();
		panel = new JPanel();

		frame.setBounds(x, y, width, height);
		frame.setContentPane(panel);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setUndecorated(true);

		panel.setBackground(Color.MAGENTA);
		frame.setVisible(true);

		hSpeed = 5;
		vSpeed = 10;

	}

	public void update(Paddle paddle1, Paddle paddle2) {

		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		Point location = frame.getLocation();
		if (location.y + 150 >= screenSize.height) {
			vSpeed = -vSpeed;
		}
		if (50 > location.y) {
			vSpeed = -vSpeed;
		}

		if (location.x + 150 > screenSize.width || location.x < 50) {
			reset(paddle1,paddle2,location, screenSize);
		}

		if (paddle1.getLocation().intersects(frame.getBounds())) {
			hSpeed = -hSpeed;
		}
		if (paddle2.getLocation().intersects(frame.getBounds())) {
			hSpeed = -hSpeed;
		}
		location.x += hSpeed;
		location.y += vSpeed;
		frame.setLocation(location);

	}

	private void reset(Paddle paddle1, Paddle paddle2,
			Point location, Dimension screenSize) {
		
		if(location.x < screenSize.width/2){
			System.out.println("RED GETS POINT");
			paddle2.addPoint();
			
		}
		if(location.x > screenSize.width/2){
			System.out.println("BLUE GETS POINT");
			paddle1.addPoint();
		}
		location.x = screenSize.width / 2;
		location.y = screenSize.height / 2;
	}
}
