/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package higuys;

import java.awt.Color;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.KeyListener;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author zacnoo
 */
public class Paddle {

	private JFrame frame;
	private JPanel panel;

	private boolean movingUp;
	private boolean movingDown;
	private int points;
	private JLabel pointsLabel;

	public Paddle(Color color, KeyListener listener, int x, int y, int width, int height) {
		points = 0;
		frame = new JFrame();
		panel = new JPanel();

		frame.addKeyListener(listener);

		frame.setBounds(x, y, width, height);
		frame.setContentPane(panel);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		panel.setBackground(color);
		frame.setVisible(true);
		panel.add(pointsLabel = new JLabel());

	}

	public void addPoint() {
		points++;
	}

	public void update() {
		if (isMovingUp()) {
			Point location = frame.getLocation();
			location.y -= 5;
			frame.setLocation(location);
		}
		if (isMovingDown()) {
			Point location = frame.getLocation();
			location.y += 5;
			frame.setLocation(location);
		}
		pointsLabel.setText("" + points);
	}

	public Rectangle getLocation() {
		return frame.getBounds();
	}

	public boolean isMovingUp() {
		return movingUp;
	}

	public void setMovingUp(boolean movingUp) {
		this.movingUp = movingUp;
	}

	public boolean isMovingDown() {
		return movingDown;
	}

	public void setMovingDown(boolean movingDown) {
		this.movingDown = movingDown;
	}

}
