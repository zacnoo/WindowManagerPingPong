/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package higuys;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author zacnoo
 */
public class HiGuys {

	Paddle paddle1, paddle2;
	Ball ball;

	/**
	 * @param args the command line arguments
	 */
	public HiGuys() throws InterruptedException {
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

		KeyListener listener = new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_W) {
					paddle1.setMovingUp(false);
				}
				if (e.getKeyCode() == KeyEvent.VK_S) {
					paddle1.setMovingDown(false);
				}
				if (e.getKeyCode() == KeyEvent.VK_UP) {
					paddle2.setMovingUp(false);
				}
				if (e.getKeyCode() == KeyEvent.VK_DOWN) {
					paddle2.setMovingDown(false);
				}
			}

			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_W) {
					paddle1.setMovingUp(true);
				}
				if (e.getKeyCode() == KeyEvent.VK_S) {
					paddle1.setMovingDown(true);
				}
				if (e.getKeyCode() == KeyEvent.VK_UP) {
					paddle2.setMovingUp(true);
				}
				if (e.getKeyCode() == KeyEvent.VK_DOWN) {
					paddle2.setMovingDown(true);
				}
			}

		};
		ball = new Ball(screenSize.width / 2, screenSize.height / 2, 100, 100);
		paddle1 = new Paddle(Color.BLUE,listener, 100, 100, 50, 300);
		paddle2 = new Paddle(Color.RED,listener, screenSize.width - 150, 100, 50, 300);
		while (true) {
			paddle1.update();
			paddle2.update();
			ball.update(paddle1,paddle2);
			Thread.sleep(30);
		}
	}

	public static void main(String[] args) throws InterruptedException {
		// TODO code application logic here
		HiGuys hg = new HiGuys();

	}

}
